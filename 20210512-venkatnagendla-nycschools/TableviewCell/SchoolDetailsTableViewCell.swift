//  Created by Venkata Nagendla on 5/12/21.
//  Copyright © 2021 Nagendla, Venkataha. All rights reserved.
//

import UIKit

class SchoolDetailsTableViewCell: UITableViewCell {
    
    let title_Label:UILabel = {
        let header = UILabel()
        header.font = UIFont.mainFontWithSFMedium()
        header.textColor = UIColor.lightGray
        header.numberOfLines = 0
        header.translatesAutoresizingMaskIntoConstraints = false
        header.textAlignment = NSTextAlignment.left
        
        return header
    }()
    
    let sub_Label:UILabel = {
        let header = UILabel()
        header.font = UIFont.mainFontWithSFMedium()
        header.textColor = UIColor.black
        header.translatesAutoresizingMaskIntoConstraints = false
        header.textAlignment = NSTextAlignment.left

        return header
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(title_Label)
        self.contentView.addSubview(sub_Label)
        
        title_Label.topAnchor.constraint(equalTo: contentView.topAnchor, constant:10).isActive = true
        title_Label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant:10).isActive = true
        title_Label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant:-10).isActive = true
        title_Label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        sub_Label.topAnchor.constraint(equalTo: contentView.topAnchor, constant:10).isActive = true
        sub_Label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant:-10).isActive = true
        sub_Label.widthAnchor.constraint(greaterThanOrEqualToConstant: 100)
        sub_Label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
}
