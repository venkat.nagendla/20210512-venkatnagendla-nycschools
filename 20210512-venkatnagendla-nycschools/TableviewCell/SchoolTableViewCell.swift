//  Created by Venkata Nagendla on 5/12/21.
//  Copyright © 2021 Nagendla, Venkataha. All rights reserved.
//


import UIKit

class SchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addFonts()
    }
    
    
    static var reuseIdentifier: String {
        
        return String(describing: self)
    }
    
   func addFonts()
    {
        nameLabel.font = UIFont.mainFontWithSFMedium()
        phoneNumber.font = UIFont.mainFontWithSFMedium()
        cityLabel.font = UIFont.mainFontWithSFMedium()
        emailLabel.font = UIFont.mainFontWithSFMedium()
        websiteLabel.font = UIFont.mainFontWithSFMedium()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
