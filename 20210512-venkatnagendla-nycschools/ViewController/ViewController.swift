//
//  ViewController.swift
//  Created by Venkata Nagendla on 5/12/21.
//  Copyright © 2021 Nagendla, Venkataha. All rights reserved.
//


import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var schoolsTableView: UITableView!
    var schoolsList  = Array<Any>()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //Calling services to retrieve list of schools
        downloadSchoolsList()
        WebService.shared.downloadSatScores()
    }
    
    func downloadSchoolsList(){
        //Displaying loader untill receives the api response
        showLoading()
        let url         = URL(string: GlobalVariables.schoolsListUrl)!
        let urlRequest  = URLRequest(url: url )
        let dataTask    = URLSession.shared.dataTask(with: urlRequest) { (Data, URLResponse, Error) in
            if(!(Error != nil)){
                do{
                    //validating response
                    let schoolsArray = try JSONSerialization.jsonObject(with: Data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    self.schoolsList = schoolsArray as! Array;
                    DispatchQueue.main.async { // Correct
                        self.hideLoading()
                        //reloading tableview
                        self.schoolsTableView.reloadData()
                    }
                    
                }catch{
                    
                }
            }
        }
        
        dataTask.resume()
    }
    
    func showLoading(){
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification?.detailsLabelText = "Please Wait..."
        loadingNotification?.detailsLabelFont = loadingNotification?.labelFont
        self.view.addSubview(loadingNotification!)
        loadingNotification?.show(true)
    }
    
    func hideLoading(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}

extension ViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SchoolTableViewCell = tableView.dequeueReusableCell(withIdentifier: GlobalVariables.schoolsListCellId) as! SchoolTableViewCell
        let schoolObj: AnyObject    = schoolsList[indexPath.row] as AnyObject
        cell.nameLabel.attributedText     =  AppUtility.increaseTheSpacing(forTheText: (schoolObj[GlobalVariables.schoolName]! as! String))
        cell.cityLabel.attributedText     = AppUtility.increaseTheSpacing(forTheText: (schoolObj[GlobalVariables.schoolsCity]! as! String))
        cell.phoneNumber.attributedText   = AppUtility.increaseTheSpacing(forTheText: (schoolObj[GlobalVariables.schoolNmber]! as! String))
        cell.emailLabel.attributedText    = AppUtility.increaseTheSpacing(forTheText: ((schoolObj[GlobalVariables.schoolsEmail]) ?? "" ) as? String)
        cell.websiteLabel.attributedText  = AppUtility.increaseTheSpacing(forTheText: (schoolObj[GlobalVariables.schoolsWebsite]! as! String))
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let satDetailsVC : SchoolsSatDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: GlobalVariables.satDetailsViewId) as! SchoolsSatDetailsViewController
        
        satDetailsVC.schoolObject       = schoolsList[indexPath.row] as AnyObject
        let schoolObj: AnyObject        = schoolsList[indexPath.row] as AnyObject
        //filtering the sat list array
        let satArray = WebService.shared.schoolsSATscoresArray.filter { (value) -> Bool in
            let satObject : AnyObject = value as AnyObject
            let dbn         = satObject[GlobalVariables.dbn] as! String
            let schoolDbn   = schoolObj[GlobalVariables.dbn] as! String
            if(dbn == schoolDbn){
                return true
            }else{
                return false
            }
        }
        //print("the test array is \(satArray)");
        if(satArray.count > 0){
            satDetailsVC.schoolSatObject    = satArray[0] as AnyObject
        }
        self.navigationController?.pushViewController(satDetailsVC, animated: true)
    }
}

