//  Created by Venkata Nagendla on 5/12/21.
//  Copyright © 2021 Nagendla, Venkataha. All rights reserved.
//

import UIKit

class AppUtility: NSObject {
    
    static let sharedInstance = AppUtility()
    
    class func increaseTheSpacing(forTheText text:String?) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: text ?? "", attributes:[NSAttributedString.Key.kern: 0.6])
        
        return attributedString
    }
}
