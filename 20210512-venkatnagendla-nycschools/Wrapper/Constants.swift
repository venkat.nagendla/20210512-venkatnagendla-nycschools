//
//  Created by Venkata Nagendla on 5/12/21.
//  Copyright © 2021 Nagendla, Venkataha. All rights reserved.
//


import UIKit

enum Constants {

    enum fontSizes:Float {
        case HEADER_FONT = 16.0
        case MAIN_FONT   = 14.0
        case SUB_FONT    = 12.0
    }
    
    enum cellHeight:Float {
        case standardCellHeight = 45.0
    }
}
