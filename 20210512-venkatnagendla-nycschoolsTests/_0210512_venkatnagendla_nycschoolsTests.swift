//
//  _0210512_venkatnagendla_nycschoolsTests.swift
//  20210512-venkatnagendla-nycschoolsTests
//
//  Created by Venkata Nagendla on 5/12/21.
//

import XCTest
@testable import _0210512_venkatnagendla_nycschools

class _0210512_venkatnagendla_nycschoolsTests: XCTestCase {

    func testNYCSchoolListApiCallCompletes() throws {
      // given
      let urlString = GlobalVariables.schoolsListUrl
      let url = URL(string: urlString)!
      let promise = expectation(description: "Completion handler invoked")
      var statusCode: Int?
      var responseError: Error?

      // when
      let dataTask = URLSession.shared.dataTask(with: url) { _, response, error in
        statusCode = (response as? HTTPURLResponse)?.statusCode
        responseError = error
        promise.fulfill()
      }
      dataTask.resume()
      wait(for: [promise], timeout: 5)

      // then
      XCTAssertNil(responseError)
      XCTAssertEqual(statusCode, 200)
    }
    
    func testSchoolSatScoresApiCallCompletes() throws {
      // given
      let urlString = GlobalVariables.schoolsSatListUrl
      let url = URL(string: urlString)!
      let promise = expectation(description: "Completion handler invoked")
      var statusCode: Int?
      var responseError: Error?

      // when
      let dataTask = URLSession.shared.dataTask(with: url) { _, response, error in
        statusCode = (response as? HTTPURLResponse)?.statusCode
        responseError = error
        promise.fulfill()
      }
      dataTask.resume()
      wait(for: [promise], timeout: 5)

      // then
      XCTAssertNil(responseError)
      XCTAssertEqual(statusCode, 200)
    }
}
